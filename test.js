class RoaringBitmap {
    /*constructor(str) {
        const strdecoded = atob(str);
        const u8array = new Uint8Array(strdecoded.length);
        for (let j = 0; j < strdecoded.length; ++j) {
            u8array[j] = strdecoded.charCodeAt(j);
        }*/
    constructor(buffer) {
        const u8array = new Uint8Array(buffer);
        const has_runs = u8array[0] === 0x3b;
        const size = has_runs ?
            ((u8array[2] | (u8array[3] << 8)) + 1) :
            ((u8array[4] | (u8array[5] << 8) | (u8array[6] << 16) | (u8array[7] << 24)));
        let i = has_runs ? 4 : 8;
        let is_run;
        if (has_runs) {
            const is_run_len = Math.floor((size + 7) / 8);
            is_run = u8array.slice(i, i + is_run_len);
            i += is_run_len;
        } else {
            is_run = new Uint8Array();
        }
        this.keys = [];
        this.cardinalities = [];
        for (let j = 0; j < size; ++j) {
            this.keys.push(u8array[i] | (u8array[i + 1] << 8));
            i += 2;
            this.cardinalities.push((u8array[i] | (u8array[i + 1] << 8)) + 1);
            i += 2;
        }
        this.containers = [];
        let offsets = null;
        if (!has_runs || this.keys.length >= 4) {
            offsets = [];
            for (let j = 0; j < size; ++j) {
                offsets.push(u8array[i] | (u8array[i + 1] << 8) | (u8array[i + 2] << 16) |
                    (u8array[i + 3] << 24));
                i += 4;
            }
        }
        for (let j = 0; j < size; ++j) {
            if (offsets && offsets[j] !== i) {
                console.log(this.containers);
                throw new Error(`corrupt bitmap ${j}: ${i} / ${offsets[j]}`);
            }
            if (is_run[j >> 3] & (1 << (j & 0x7))) {
                const runcount = (u8array[i] | (u8array[i + 1] << 8));
                i += 2;
                this.containers.push(new RoaringBitmapRun(
                    runcount,
                    u8array.slice(i, i + (runcount * 4)),
                ));
                i += runcount * 4;
            } else if (this.cardinalities[j] >= 4096) {
                this.containers.push(new RoaringBitmapBits(u8array.slice(i, i + 8192)));
                i += 8192;
            } else {
                const end = this.cardinalities[j] * 2;
                this.containers.push(new RoaringBitmapArray(
                    this.cardinalities[j],
                    u8array.slice(i, i + end),
                ));
                i += end;
            }
        }
    }
    contains(keyvalue) {
        const key = keyvalue >> 16;
        const value = keyvalue & 0xFFFF;
        for (let i = 0; i < this.keys.length; ++i) {
            if (this.keys[i] === key) {
                return this.containers[i].contains(value);
            }
        }
        return false;
    }
}

class RoaringBitmapRun {
    constructor(runcount, array) {
        this.runcount = runcount;
        this.array = array;
    }
    contains(value) {
        const l = this.runcount * 4;
        for (let i = 0; i < l; i += 4) {
            const start = this.array[i] | (this.array[i + 1] << 8);
            const lenm1 = this.array[i + 2] | (this.array[i + 3] << 8);
            if (value >= start && value <= (start + lenm1)) {
                return true;
            }
        }
        return false;
    }
}
class RoaringBitmapArray {
    constructor(cardinality, array) {
        this.cardinality = cardinality;
        this.array = array;
    }
    contains(value) {
        const l = this.cardinality * 2;
        for (let i = 0; i < l; i += 2) {
            const start = this.array[i] | (this.array[i + 1] << 8);
            if (value === start) {
                return true;
            }
        }
        return false;
    }
}
class RoaringBitmapBits {
    constructor(array) {
        this.array = array;
    }
    contains(value) {
        return !!(this.array[value >> 3] & (1 << (value & 7)));
    }
}

const fs = require("fs");
let rb = new RoaringBitmap(fs.readFileSync("testdata/bitmapwithoutruns.bin"));
for (let k = 0; k < 100000; k+= 1000) {
    if (!rb.contains(k)) {
        throw new Error(`Does not contain ${k}, but it should`);
    }
}
for (let k = 100000; k < 200000; ++k) {
    if (!rb.contains(3*k)) {
        throw new Error(`Does not contain ${3*k}, but it should`);
    }
}
for (let k = 700000; k < 800000; ++k) {
    if (!rb.contains(k)) {
        throw new Error(`Does not contain ${k}, but it should`);
    }
}

rb = new RoaringBitmap(fs.readFileSync("testdata/bitmapwithruns.bin"));
for (let k = 0; k < 100000; k+= 1000) {
    if (!rb.contains(k)) {
        throw new Error(`Does not contain ${k}, but it should`);
    }
}
for (let k = 100000; k < 200000; ++k) {
    if (!rb.contains(3*k)) {
        throw new Error(`Does not contain ${3*k}, but it should`);
    }
}
for (let k = 700000; k < 800000; ++k) {
    if (!rb.contains(k)) {
        throw new Error(`Does not contain ${k}, but it should`);
    }
}